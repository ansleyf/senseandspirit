<?php
    // Menu Registration
    register_nav_menus(array(
        'primary_nav' => 'Main Header Navigation',
        'footer_nav' => 'Footer Navigation'
    ));

    // Side bar Registration
    add_action( 'widgets_init', 'regsiter_theme_sidebars' );
    function regsiter_theme_sidebars() {
        register_sidebar(
            array(
                'id' => 'primary',
                'name' => __( 'Primary' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>'
            )
        );
    }

	
    
    // Misc registrations
    add_theme_support('post-thumbnails');
    add_image_size("product-thumb", 300, 300, TRUE);
    add_image_size("slider-image", 1150, 400, TRUE);

    // ShortenText
	function ShortenText($text) { // Function name ShortenText
        $chars_limit = 25; // Character length
        $chars_text = strlen($text);
        $text = $text." ";
        $text = substr($text,0,$chars_limit);
        $text = substr($text,0,strrpos($text,' '));

        if ($chars_text > $chars_limit)
        { $text = $text."..."; } // Ellipsis
        return $text;
	}
    function related_products() {
        global $id;
        $category = get_the_category($id);
        $args = array(
            "post_type"         => "product",
            "category_name"     => $category[0]->cat_name,
            "posts_per_page"    => 3,

        );
        query_posts($args);
        echo '<ul class="related-products">';
        echo '<h2>Related Products</h2>';
        while(have_posts()) : the_post();
        echo '<li><a href="'.get_permalink().'">';
            echo '<div class="uk-display-block uk-width-1-1">';
                the_post_thumbnail(array(75,75),array("class"=>"uk-align-left"));
                the_title();
                echo '<span class="uk-display-block uk-margin-remove">';
                mp_product_price();
                echo '</span>';
            echo '</div> <div class="uk-clearfix"></div>';
        echo '</a></li>';
        endwhile;
        echo '</ul>';  
    }


    // Menu Walker Class
    class themeslug_walker_nav_menu extends Walker_Nav_Menu {
    // add classes to ul sub-menus
    function start_lvl( &$output, $depth ) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu uk-nav uk-nav-navbar',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
            );
        $class_names = implode( ' ', $classes );

        // build html
        $output .= "\n" . $indent . '<div class="uk-dropdown uk-dropdown-navbar"><ul class="' . $class_names . '">' . "\n";
    }

    // add main/sub classes to li's and links
     function start_el( &$output, $item, $depth, $args ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // passed classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        if(strpos($class_names, 'menu-item-has-children')) {$haschild = "data-uk-dropdown"; }
        // build html
        $output .= $indent . '<li '. $haschild .' id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );

        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}