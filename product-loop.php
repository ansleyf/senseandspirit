<div class="uk-panel uk-panel-box">
	<a class="no-deco" href="<?php the_permalink(); ?>">
		<div class="uk-panel-teaser">
			<?php the_post_thumbnail("product-thumb"); ?>
		</div>
		<div class="uk-panel-title"><?php mp_product_title($post->ID); ?></div>
	</a>
	<div class="uk-float-left uk-text-small"><?php mp_product_price(); ?></div>
	<div class="uk-float-right"><a class="uk-button uk-button-primary" href="<?php the_permalink(); ?>">View Product <i class="uk-icon-caret-right"></i></a></div>
	<div class="uk-clearfix"></div>
</div>