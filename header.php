<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <link href="<?php echo get_template_directory_uri(); ?>/css/uikit.almost-flat.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/css/addons/uikit.almost-flat.addons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/css/bxslider.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
       
	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/uikit.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/addons/jquery.fitvids.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>

       <script type="text/javascript" src="//use.typekit.net/ycn0jyi.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

        <link rel="shortcut icon" href="/favicon.ico">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
		<?php
	        $menuargs = array(
	            "container"         => "",
	            "theme_location"    => "primary_nav",
	            "menu_class"        => "uk-subnav uk-visible-large",
	            "menu_id"           => "",
	            // 'walker'            => new themeslug_walker_nav_menu
            );
            $offcanvnav = array(
	            "container"         => "",
	            "theme_location"    => "primary_nav",
	            "menu_class"        => "uk-nav uk-nav-offcanvas",
	            "menu_id"           => ""
        	);
        ?>
    <header class="uk-width-1-1">
	    <div id="offcanv" class="uk-offcanvas">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
				<?php wp_nav_menu($offcanvnav); ?>
			</div>
		</div>
		<div class="uk-container uk-container-center">
			<div class="logo-box">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php echo wp_get_attachment_url(874); ?>" alt="<?php echo get_bloginfo( "name", "raw"); ?>" />
				</a>
			</div>
			<div class="uk-float-right uk-margin-top">
				<?php wp_nav_menu($menuargs); ?>
			</div>
			<div class="uk-clearfix"></div>
		</div>
		<nav class="uk-navbar uk-navbar-attached">
			<div class="uk-navbar-center uk-hidden-small">
				<?php echo get_bloginfo('description', 'raw'); ?>
			</div>
			<div class="cart">
				<?php echo do_shortcode( '[mpawc design="link" cartstyle="dropdown-click" onlystorepages="no" btncolor="grey"]' ); ?>
			</div>
			<div class="uk-navbar-flip">
			</div>
			<div class="uk-clearfix"></div>
		</nav>
		<a href="#offcanv" class="uk-navbar-toggle uk-float-right uk-margin-right uk-hidden-large" data-uk-offcanvas></a>
		<div class="uk-clearfix"></div>
    </header>