<?php
/*
	Template Name: Community
*/
	get_header();
?>
	<div class="uk-container uk-container-center community">
		<div class="uk-grid uk-margin-top">
			<div class="uk-width-large-7-10">
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					"posts_per_page"	=> get_option("posts_per_page"),
					"paged"				=> $paged
				);
				query_posts($args);
				while (have_posts()) : the_post();
			?>
				<article class="uk-article">
					<h2 class="uk-article-title uk-margin-bottom-remove"><a class="no-deco" href="<?php the_permalink() ;?>"><?php the_title(); ?></a></h2>
					<div class="uk-article-meta uk-margin-bottom">Posted on <?php the_time(get_option('date_format')); ?></div>
					<?php the_content(); ?>
				</article>
			<?php endwhile; ?>
			<ul class="uk-pagination uk-margin-top">
				<li class="uk-pagination-previous"><?php previous_posts_link("Newer Entries"); ?></li>
				<li class="uk-pagination-next"><?php next_posts_link("Older Entries"); ?></li>
			</ul>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();