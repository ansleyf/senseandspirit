<article class="uk-article product-page">
	<h1 class="uk-article-title uk-margin-remove"><?php the_title(); ?></h1>
	<div class="uk-article-meta">in <span><?php echo mp_category_list($post->ID); ?></span></div>
	<div class="uk-grid uk-margin-top">
		<div class="uk-width-medium-1-3">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="uk-width-medium-2-3 uk-text-center">
			<div class="uk-display-block uk-width-1-1 uk-margin-large-top uk-margin-bottom price"><?php mp_product_price(); ?></div>
			<div class="uk-display-block uk-width-1-1 addtocart"><?php mp_buy_button(true, 'single'); ?></div>
		</div>
		<div class="uk-width-1-1 content uk-margin-top">
			<?php mp_product_description($post->ID); ?>
		</div>
	</div>
</article>