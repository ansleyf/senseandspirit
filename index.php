<?php
	get_header();
?>
<div class="uk-container uk-container-center uk-margin-large-top">
	<div class="uk-width-1-1 uk-margin-bottom">
		<ul class="bxslider">
			<?php
				$slider = array(
					"post_type"			=> "slide",
					"posts_per_page"	=> 5,
				);
				query_posts($slider);
				while (have_posts()) : the_post(); $do_not_duplicate[] = get_the_ID();
				$image = get_post_meta($post->ID, "image", TRUE);
			?>
				<li>
					<?php echo wp_get_attachment_image($image["ID"],"slider-image",array("class"=>"uk-responsive-height uk-margin-bottom-remove")); ?>
					<div class="overlay">
						<a href="<?php echo get_post_meta($post->ID, "link", TRUE); ?>"><h2><?php the_title(); ?></h2>
						<?php echo get_the_excerpt(); ?></a>
					</div>
				</li>
			<?php endwhile; wp_reset_query(); ?>
		</ul>
		<script>
			$(document).ready(function() {
				$('.bxslider').bxSlider({
					mode: 'fade',
					auto: true,
					autoHover: true,
					useCSS: false
				});
			});
		</script>
	</div>
	<div class="uk-width-1-1">
		<h2 class="fav-products uk-text-center">Favorite Products</h2>
	</div>
	<ul class="uk-grid fav-products" data-uk-grid-margin data-uk-grid-match="{target:'.uk-panel', row: true}">
	<?php
		$args = array(
			'post_type' => 'product',
			'product_category' => 'favorites',
			'posts_per_page' => 8
		);
		$wp_query = new WP_Query($args);
		if ( have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
	?>
		<li class="uk-width-medium-1-4">
			<?php get_template_part("product","loop_other"); ?>
		</li>
	<?php endwhile; endif; wp_reset_query(); ?>
	</ul>
</div>
<?php
	get_footer();