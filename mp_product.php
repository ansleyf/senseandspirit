<?php get_header(); ?>
<div class="uk-container uk-container-center uk-margin-large-top">
	<div class="uk-grid">
		<div class="uk-width-large-7-10">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part("product", "single"); ?>
			<?php endwhile; ?>
		</div>
		<div class="uk-width-3-10 uk-visible-large">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>