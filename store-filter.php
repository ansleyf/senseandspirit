<form id="filter" class="uk-form uk-float-right uk-display-inline-block uk-vertical-align-bottom">
	<select name="category">
		<option selected>Filter By</option>
		<?php foreach (get_terms("product_category") as $cat) : ?>
			<option value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></option>
		<?php endforeach; ?>
	</select>
</form>
<script>
	$('#filter select').change(function(event) {
		event.preventDefault();
		var name = $('#filter option:selected').val();
		window.location = '<?php echo home_url(); ?>/store/products/category/'+name;
		console.log(name);
	});
</script>