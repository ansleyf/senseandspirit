<?php get_header(); ?>
<div class="uk-container uk-container-center">
	<?php get_template_part("store", "filter"); ?>
	<ul class="uk-grid product-list uk-margin-large-top" data-uk-grid-margin data-uk-grid-match="{target:'.uk-panel', row: true}">
	<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 8,
			'paged' => $paged
		);
		$wp_query = new WP_Query($args);
		if ( have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
	?>
		<li class="uk-width-medium-1-4">
			<?php get_template_part("product", "loop_other"); ?>
		</li>
	<?php endwhile; ?>
	</ul>
		<ul class="uk-pagination uk-margin-top">
			<li class="uk-pagination-previous"><?php previous_posts_link("<i class='uk-icon-caret-left'></i> Back"); ?></li>
			<li class="uk-pagination-next"><?php next_posts_link("More <i class='uk-icon-caret-right'></i>"); ?></li>
		</ul>
		<div class="uk-clearfix"></div>
	<?php endif; ?>
</div>
<?php get_footer(); ?>