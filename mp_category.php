<?php get_header(); ?>
<div class="uk-container uk-container-center">
	<div class="uk-grid">
		<div class="uk-width-medium-2-3">
			<h1 class="dark-pink uk-margin-large-top">Browsing: <?php single_cat_title(); ?></h1>
		</div>
		<div class="uk-width-medium-1-3">
			<?php get_template_part("store", "filter"); ?>
		</div>
	</div>
	<ul class="uk-grid product-list" data-uk-grid-margin data-uk-grid-match="{target:'.uk-panel', row: true}">
	<?php
		if (have_posts()) : while (have_posts()) : the_post();
	?>
		<li class="uk-width-medium-1-4">
			<?php get_template_part("product", "loop_other"); ?>
		</li>
	<?php endwhile; ?>
	</ul>
		<ul class="uk-pagination uk-margin-top">
			<li class="uk-pagination-previous"><?php previous_posts_link("<i class='uk-icon-caret-left'></i> Back"); ?></li>
			<li class="uk-pagination-next"><?php next_posts_link("More <i class='uk-icon-caret-right'></i>"); ?></li>
		</ul>
		<div class="uk-clearfix"></div>
	<?php endif; ?>
</div>
<?php get_footer(); ?>