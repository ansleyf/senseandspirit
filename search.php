<?php get_header(); ?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid uk-margin-top">
			<div class="uk-width-large-7-10 headerpush">
			<?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
			<h1>Search Results for: <?php echo get_search_query() ?></h1>
				<article class="uk-article">
					<h2 class="uk-article-title uk-margin-bottom-remove"><a class="no-deco" href="<?php the_permalink() ;?>"><?php the_title(); ?></a></h2>
					<div class="uk-article-meta uk-margin-bottom"><?php the_time( get_option( 'date_format' ) ); ?> by: <?php the_author(); ?> in <?php the_category(", "); ?></div>
					<?php the_post_thumbnail("list-thumb", array("class"=>"uk-align-left")); ?>
					<?php the_excerpt(); ?> <a href="<?php the_permalink(); ?>">READ MORE</a>
				</article>
				<?php endwhile; else : ?>
				<h1 class="uk-text-center">No Results Found!</h1>
				<?php get_search_form(); ?>
				<?php endif; ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>