<footer class="uk-width-1-1 uk-margin-large-top">
	<div class="sense">
		<div class="uk-text-center uk-text-small">
			<ul>
			<li><a href="<?php echo get_permalink(508); ?>">Contact Us</a></li>
			<li><a href="<?php echo get_permalink(707); ?>" >Privacy Policy</a></li>
			<li><a href="<?php echo get_permalink(705); ?>">Terms of Use</a></li>
			</ul>
		</div>
	</div>
	<div class="af">
		<div class="uk-text-center uk-text-small">
			&copy; 2014 <?php echo get_bloginfo( "name", "raw"); ?>. All Rights Reserved. &bull; <a href="http://ansleyfones.com/">Design &amp; Development by <img src="<?php echo get_template_directory_uri(); ?>/images/af-logo-very-small.png" alt="AF" /> Ansley Fones</a>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>