<div class="uk-panel panel-product">
	<a class="no-deco" href="<?php the_permalink(); ?>">
		<div class="product-teaser">
			<?php the_post_thumbnail("product-thumb",array("class"=>"uk-responsive-width uk-align-center uk-margin-bottom-remove")); ?>
		</div>
		<div class="product-title"><?php echo ShortenText(mp_product_title($post->ID)); ?></div>
	</a>
	<div class="product-price uk-text-small"><?php mp_product_price(); ?></div>
	<a href="<?php the_permalink(); ?>"><div class="product-addtocart">
		
			View Product <i class="uk-icon-caret-right"></i>
		
	</div></a>
	<div class="uk-clearfix"></div>
</div>